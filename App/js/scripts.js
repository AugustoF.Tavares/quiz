$(function(){
    limpaPaginas();
});

function limpaPaginas(){
    $("#root").css("display", "block");
    $( "#root" ).children().css( "display", "none" );
}

function mudaParaPagina(elem) {
    limpaPaginas();
    $(elem).css("display", "block");
}

const app = document.getElementById("root");

const tituloPagina = document.getElementById("titulo");

// Cria uma variável de requisição e atribui uma instancia de XMLHttpRequest object to it.
var request = new XMLHttpRequest();

//Inicia a pergunta atual do o quiz com a primeira pergunta (array indice 0)
var perguntaAtual = 0;

var perguntas = new Array();
var respostaCorreta = "";
var quantidadeAcertos = 0;

function init(){
    abrirPaginaInicial();
}