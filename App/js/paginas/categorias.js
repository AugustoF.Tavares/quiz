function selecioneCategoria() {
    tituloPagina.textContent = "Categorias";
    // Abre uma nova conexão, usando a requisição GET na URL
    request.open('GET', 'http://localhost/quizapp/quiz/Api/categoria/ler.php', true);

    request.onload = function () {
        // Começa acessando os dados do JSON aqui
        categorias = JSON.parse(this.response);
        
        const containerCategoria = document.getElementById('botoes');
        containerCategoria.innerHTML = "";
        if (request.status >= 200 && request.status < 400) {
            categorias.forEach(categoria => {

                //Adiciona os botões de perguntas
                var categoriaItem = document.createElement('button');
                    categoriaItem.setAttribute('class', 'botao_quiz');
                    categoriaItem.setAttribute('onclick', 'javascript:iniciaQuiz('+categoria.id_categoria+')');
                    categoriaItem.setAttribute('name', 'id');
                var spanCategoria = document.createElement('span');
                    spanCategoria.textContent = categoria.descricao;;
                
                    categoriaItem.appendChild(spanCategoria);
                    containerCategoria.appendChild(categoriaItem);
            });
        }
    }
    // Envia a requisição
    request.send();
    mudaParaPagina("#pagina-categorias");
}