function abrirLogin(){
    mudaParaPagina("#pagina-login");
    tituloPagina.textContent = "Login";
}

function login(){
    var email = document.getElementById("email").value;
    var senha = document.getElementById("senha").value;

    var mensagemErro = document.getElementById('mensagem-erro');

     // Abre uma nova conexão, usando a requisição GET na URL
     request.open('GET', 'http://localhost/quizapp/quiz/Api/usuario/verifica_login.php?email='+email+'&senha='+senha, true);

     request.onload = function () {
         // Começa acessando os dados do JSON aqui
         resultado = JSON.parse(this.response);

         if(resultado.codigo == 1) {
            abrirFormularioDeCadastro();
         } else {
            mensagemErro.textContent = resultado.mensagem;
         }
     }
     // Envia a requisição
     request.send();
}