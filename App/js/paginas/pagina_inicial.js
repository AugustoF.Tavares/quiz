function abrirPaginaInicial() {
    tituloPagina.textContent = "Quiz";
    // Abre uma nova conexão, usando a requisição GET na URL
    request.open('GET', 'http://localhost/quizapp/quiz/Api/partida/ranking.php', true);

    request.onload = function () {
        // Começa acessando os dados do JSON aqui
        ranking = JSON.parse(this.response);
        
        const containerRanking = document.getElementById('ranking');
        containerRanking.innerHTML = "";
        if(ranking.codigo != null && ranking.codigo == 0){
            var emptyRanking = '<tr><td colspan="3">'+ ranking.mensagem +'</td></tr>';
            containerRanking.insertAdjacentHTML('beforeend', emptyRanking);
        } else {
            var headerRanking = '<tr><td colspan="3">Top 10 jogadores</td></tr><tr><td>Rank</td><td>Jogador</td><td>Score</td></tr>';
            containerRanking.insertAdjacentHTML('beforeend', headerRanking);
            var indice = 1;

            if (request.status >= 200 && request.status < 400) {
                ranking.forEach(score => {
                    
                    //Adiciona os botões de perguntas
                    var scoreLine = document.createElement('tr');
                    
                    var colunaPosicao = document.createElement('td');
                    colunaPosicao.textContent = indice;
                    scoreLine.appendChild(colunaPosicao);
                    
                    var colunaNomeJogador = document.createElement('td');
                    colunaNomeJogador.textContent = score.nome_jogador;
                    scoreLine.appendChild(colunaNomeJogador);

                    var colunaScore = document.createElement('td');
                    colunaScore.textContent = score.soma_acertos;
                    scoreLine.appendChild(colunaScore);

                    containerRanking.appendChild(scoreLine);
                    indice = indice + 1;
                });
            }
        }
    }
    // Envia a requisição
    request.send();
    console.log("pagina inicial");
    mudaParaPagina("#pagina-inicial");
}

function iniciarPartida() {
    console.log("iniciou");
    var nome = document.getElementById("nomejogador").value;

    var mensagemErroPartida = document.getElementById('mensagem-erro-partida');

     // Abre uma nova conexão, usando a requisição GET na URL
     request.open('GET', 'http://localhost/quizapp/quiz/Api/jogador/cadastrar.php?nome='+nome, true);

     request.onload = function () {
         // Começa acessando os dados do JSON aqui
         resultado = JSON.parse(this.response);

         if(resultado.codigo == 1) {
            var idjogadoratual = document.getElementById("id_jogador_atual");
            idjogadoratual.value = resultado.id_jogador;
            console.log(idjogadoratual.value);
            selecioneCategoria();
         } else {
            mensagemErroPartida.textContent = resultado.mensagem;
         }
     }
     // Envia a requisição
     request.send();
}