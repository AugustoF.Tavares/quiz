
function abrirFormularioDeCadastro() {
    mudaParaPagina("#pagina-cadastro")
    tituloPagina.textContent = "Cadastrar Pergunta";

    $(document).on('click', '.input-choice', function () {
        $(".input-choice").removeClass("selected");
        $(this).addClass("selected");
        $("#certa-resposta").val($(this).attr("value"));
    });

    request.open('GET', 'http://localhost/quizapp/quiz/Api/categoria/ler.php', true);

    request.onload = function () {
        // Começa acessando os dados do JSON aqui
        listaCategorias = JSON.parse(this.response);
        
        const grupoCategoria = document.getElementById('grupo-categoria');
        grupoCategoria.innerHTML = "";
        if (request.status >= 200 && request.status < 400) {
            listaCategorias.forEach(categoria => {

                var categoriaAttrId = 'categoria-'+categoria.id_categoria;

                //Adiciona os botões de perguntas
                var containerCategoria = document.createElement('div');
                    containerCategoria.setAttribute('id', 'container-' + categoriaAttrId)
                    containerCategoria.setAttribute('class', 'floatBlock');

                var labeCategoria = document.createElement('label');
                    labeCategoria.setAttribute('for', categoriaAttrId);

                var categoriaInput = document.createElement('input');
                    categoriaInput.setAttribute('id', categoriaAttrId);
                    categoriaInput.setAttribute('name', 'categoria_id');
                    categoriaInput.setAttribute('type', 'radio');
                    categoriaInput.setAttribute('value', categoria.id_categoria);

                
                    labeCategoria.textContent = categoria.descricao;;
                    labeCategoria.insertAdjacentElement('afterbegin' , categoriaInput);

                    containerCategoria.appendChild(labeCategoria);
                    grupoCategoria.appendChild(containerCategoria);
            });
        }
    }
    // Envia a requisição
    request.send();
}

function cadastrarPergunta(){
    var request = new XMLHttpRequest();
    var pergunta = $('#pergunta-cad').serialize();

    console.log('http://localhost/quizapp/quiz/Api/pergunta_resposta/cadastrar.php?' + pergunta);

     // Abre uma nova conexão, usando a requisição GET na URL
     request.open('GET', 'http://localhost/quizapp/quiz/Api/pergunta_resposta/cadastrar.php?' + pergunta, true);

     request.onload = function () {
         // Começa acessando os dados do JSON aqui
         resultado = JSON.parse(this.response);

         if(resultado.codigo == 1) {
             alert(resultado.mensagem);
             abrirFormularioDeCadastro();
             document.getElementById("pergunta-cad").reset();
             $(".input-choice").removeClass("selected");
         } else {
            alert(resultado.mensagem);
         }
     }
     // Envia a requisição
     request.send();
}

function sairCadastro(){
    location.reload();
}