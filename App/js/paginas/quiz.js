
//Método chamado quando a página e carregada
function iniciaQuiz(categoria_id) {
    var idJogadorAtual = document.getElementById("id_jogador_atual");
    console.log(idJogadorAtual);
    criarPartida(categoria_id);
    mudaParaPagina("#pagina-quiz");
    // Abre uma nova conexão, usando a requisição GET na URL
    request.open('GET', 'http://localhost/quizapp/quiz/Api/pergunta_resposta/ler.php?categoria_id='+categoria_id, true);
    request.onload = function () {
        // Começa acessando os dados do JSON aqui
        perguntas = JSON.parse(this.response);
        tituloPagina.textContent = "Quiz";
        vaParaPergunta(perguntas[perguntaAtual]);
    }
    // Envia a requisição
    request.send();
}

function criarPartida(categoria_id) {
    quantidadeAcertos = 0;
    perguntaAtual = 0
    console.log('"chegou aqui');
    var requestPartida = new XMLHttpRequest();
    var idpartidaatual = document.getElementById("id_partida");
    var idJogadorAtual = document.getElementById("id_jogador_atual").value;
     // Abre uma nova conexão, usando a requisição GET na URL
     requestPartida.open('GET', 'http://localhost/quizapp/quiz/Api/partida/criar_partida.php?idJogador='+idJogadorAtual+'&idCategoria='+categoria_id, true);

     requestPartida.onload = function () {
         // Começa acessando os dados do JSON aqui
         resultado = JSON.parse(this.response);

         if(resultado.codigo == 1) {
            idpartidaatual.value = resultado.id_partida
         }
     }
     // Envia a requisição
     requestPartida.send();
}

//Recebe click dos botões de resposta
function respostaClick(elem) {
    //Recupera valor do conteúdo do botão clicado
    var valorResposta = elem.value;

    if(valorResposta == respostaCorreta) {
        quantidadeAcertos++;
    }

    console.log(valorResposta);
    if(perguntaAtual < perguntas.length) {
        vaParaPergunta(perguntas[perguntaAtual]);
    } else {
        fimDoQuiz(quantidadeAcertos);
    }
}

function vaParaPergunta(pergunta) {
    console.log(pergunta.pergunta);
    if(request.status >= 200 && request.status < 400) {
            //Adiciona a pergunta
            $("#questao").text(pergunta.pergunta);

            //Cria o container de respostas
            var container = app.querySelector('#respostas-area');
            container.innerHTML = "";

            var resposta1 = '<button class="botao_quiz" onclick="respostaClick(this)" value="1"><span>';
                resposta1 += pergunta.resposta1;
                resposta1 += '</span></button>';
                container.insertAdjacentHTML('beforeend', resposta1);

            var resposta2 = '<button class="botao_quiz" onclick="respostaClick(this)"value="2"><span>';
                resposta2 += pergunta.resposta2;
                resposta2 += '</span></button>';
                container.insertAdjacentHTML('beforeend', resposta2);
            
            var resposta3 = '<button class="botao_quiz" onclick="respostaClick(this)"value="3"><span>';
                resposta3 += pergunta.resposta3;
                resposta3 += '</span></button>';
                container.insertAdjacentHTML('beforeend', resposta3);
            
            var resposta4 = '<button class="botao_quiz" onclick="respostaClick(this)"value="4"><span>';
                resposta4 += pergunta.resposta4;
                resposta4 += '</span></button>';
                container.insertAdjacentHTML('beforeend', resposta4);

            $('#progresso').text("Questão " + (perguntaAtual + 1) + " de " + perguntas.length);

            respostaCorreta = pergunta.resposta_certa;
    }
    perguntaAtual++;
}

function fimDoQuiz(acertos) {
    mudaParaPagina("#pagina-resultado");

    var requestFimPartida = new XMLHttpRequest();
    var idpartidaatual = document.getElementById("id_partida");
         // Abre uma nova conexão, usando a requisição GET na URL
         requestFimPartida.open('GET', 'http://localhost/quizapp/quiz/Api/partida/finalizar_partida.php?idPartida='+idpartidaatual.value+'&acertos='+acertos, true);
    
         requestFimPartida.onload = function () {
             // Começa acessando os dados do JSON aqui
        resultado = JSON.parse(this.response);
    
        if(resultado.codigo == 1) {
                idpartidaatual.value = resultado.id_partida
             }
         }
         // Envia a requisição
         requestFimPartida.send();

    tituloPagina.textContent = "Resultado";

    $("#qtd-acertos").text(acertos);

}