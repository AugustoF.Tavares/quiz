<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
     
    include_once '../config/db.php';

    $categoria_id = $_GET['categoria_id'];

    $resultado = mysqli_query($conn, "SELECT * FROM pergunta_resposta WHERE categoria_id ='$categoria_id'");

    $num = mysqli_num_rows($resultado);

    if($num > 0) {
        $perguntas_array = array();
    
        while ($row =  mysqli_fetch_assoc($resultado)){
            extract($row);
    
            $pergunta_item = array(
                "id_pergunta" => $id_pergunta,
                "pergunta" => $pergunta,
                "resposta1" => $resposta1,
                "resposta2" => $resposta2,
                "resposta3" => $resposta3,
                "resposta4" => $resposta4,
                "resposta_certa" => $resposta_certa,
                "categoria_id" => $categoria_id
            );
    
            array_push($perguntas_array, $pergunta_item);
        }
    
        echo json_encode($perguntas_array);
    } else {
        echo json_encode(array("message" => "Nenhuma pergunta encontrada.")
    );
}
?>