<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
     
    include_once '../config/db.php';

    $pergunta = $_GET['pergunta'];
    $resposta1 = $_GET['resposta1'];
    $resposta2 = $_GET['resposta2'];
    $resposta3 = $_GET['resposta3'];
    $resposta4 = $_GET['resposta4'];
    $resposta_certa = $_GET['resposta_certa'];
    $categoria_id = $_GET['categoria_id'];

    $inserePergunta = mysqli_query($conn, "INSERT INTO pergunta_resposta(pergunta, 
                                    resposta1, resposta2, resposta3, resposta4,
                                    resposta_certa, categoria_id) 
                                    VALUES('$pergunta', '$resposta1', '$resposta2', '$resposta3',
                                           '$resposta4', '$resposta_certa', '$categoria_id')");

    if ($inserePergunta) {
        echo json_encode(array(
            "codigo" => 1,
            "mensagem" => "Pergunta cadastrada com sucesso"
        ));
    } else {
        echo json_encode(array(
            "codigo" => 0,
            "mensagem" => "Falha ao cadastrar pergunta"
        ));
    }

    ?>