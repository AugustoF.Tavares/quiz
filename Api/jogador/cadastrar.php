<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
     
    include_once '../config/db.php';

    $jogador = $_GET['nome'];

    $consultaJogador = mysqli_query($conn, "SELECT * FROM jogador WHERE nome ='$jogador'");

    $num = mysqli_num_rows($consultaJogador);

    if($num != null || $num > 0) {
        $row = mysqli_fetch_assoc($consultaJogador);
        echo json_encode(array(
            "codigo" => 1,
            "id_jogador" => $row['id'],
            "mensagem" => "Jogador cadastrado com sucesso"));
    } else {
        $sql = "INSERT INTO jogador(nome) VALUES('$jogador')";

        if($conn->query($sql) === TRUE) {
            $last_id = $conn->insert_id;
            echo json_encode(array(
            "codigo" => 1,
            "id_jogador" => $last_id,
            "mensagem" => "Jogador cadastrado com sucesso"));
        } else {
            echo json_encode(array(
                "codigo" => 0,
                "mensagem" => "Falha ao cadastrar jogador"
            ));
        }
    }

    ?>