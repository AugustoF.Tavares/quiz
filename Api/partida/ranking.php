<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
     
    include_once '../config/db.php';

    $sql = "SELECT SUM(acertos) AS soma_acertos, (SELECT j.nome
                FROM jogador AS j WHERE p.id_jogador = j.id) AS nome_jogador
            FROM  partida AS p
            GROUP BY id_jogador ORDER BY soma_acertos DESC LIMIT 0, 10";

    $resultado = mysqli_query($conn, $sql);

    $num = mysqli_num_rows($resultado);

    if($num > 0) {
        $ranking_array = array();
    
        while ($row =  mysqli_fetch_assoc($resultado)){
            extract($row);
    
            $ranking_item = array(
                "soma_acertos" => $soma_acertos,
                "nome_jogador" => $nome_jogador
            );
    
            array_push($ranking_array, $ranking_item);
        }
    
        echo json_encode($ranking_array);
    } else {
        echo json_encode(array(
            "codigo" => 0,
            "mensagem" => "Ainda não tem partidas jogadas"
        ));
    }
?>