<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
     
    include_once '../config/db.php';

    $idJogador = $_GET['idJogador'];
    $idCategoria = $_GET['idCategoria'];

    $sql = "INSERT INTO partida (id_categoria, id_jogador)
                    VALUES ('$idCategoria', '$idJogador')";

    
    if($conn->query($sql) === TRUE) {
        $last_id = $conn->insert_id;
        echo json_encode(array(
            "codigo" => 1,
            "id_partida" => $last_id,
            "mensagem" => "Partida criada com sucesso"
    ));
    } else {
        echo json_encode(array(
            "codigo" => 0,
            "mensagem" => "Falha ao criar partida"
        ));
    }
    $conn->close();
    ?>