<?php

    $host = "localhost";
    $db_name = "quiz";
    $username = "root";
    $pwd = "";
    $conn;

    $conn = null;

    $conn = mysqli_connect($host,$username,$pwd, $db_name);
    mysqli_set_charset($conn, 'utf8');

    if (!mysqli_select_db($conn, "quiz")) {
        echo "Não é possível selecionar o banco de dados: " . mysqli_error();
        exit;
    }

?>