<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
     
    include_once '../config/db.php';

    $resultado = mysqli_query($conn, "SELECT * FROM categoria"); 

    $num = mysqli_num_rows($resultado);

    if($num > 0) {
        $categoria_array = array();
    
        while ($row =  mysqli_fetch_assoc($resultado)){
            extract($row);
    
            $categoria_item = array(
                "id_categoria" => $id_categoria,
                "descricao" => $descricao
            );
    
            array_push($categoria_array, $categoria_item);
        }
    
        echo json_encode($categoria_array);
    } else {
        echo json_encode(array("message" => "Nenhuma categoria encontrada.")
    );
}
?>